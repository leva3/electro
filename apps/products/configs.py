
FRONT = 'Передний'
BACK = 'Задний'
FULL = 'Полный'
AWD = 'AWD'


TYPE_DRIVE = (
    ("FRONT", FRONT),
    ("BACK", BACK),
    ("FULL", FULL),
    ("AWD", AWD)
)
